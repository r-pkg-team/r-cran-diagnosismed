Source: r-cran-diagnosismed
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 12),
               dh-r,
               r-base-dev,
               r-cran-epitools,
               r-cran-teachingdemos,
               r-cran-amore
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-diagnosismed
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-diagnosismed.git
Homepage: https://cran.r-project.org/package=DiagnosisMed
Rules-Requires-Root: no

Package: r-cran-diagnosismed
Architecture: all
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: medical diagnostic test accuracy analysis toolkit
 DiagnosisMed is a GNU R package to analyze the accuracy of data from
 diagnostic tests evaluating health conditions. It was designed to be
 used by health professionals. This package helps estimating sensitivity
 and specificity from categorical and continuous test results including
 some  evaluations of indeterminate results, or compare different
 categorical tests, and estimate reasonable cut-offs of tests and display
 it in a way commonly used by health professionals. No graphical
 interface is available yet.
